import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';
import Swol from 'sweetalert2';

export default function Login(){

  // Allows us to consume the User context object and its properties to use for user validation
  const { user, setUser } = useContext(UserContext);
    console.log(user)
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isActive, setIsActive] = useState(false);

    // Check if values are successfully binded
    console.log(email);
    console.log(password);

    function authenticate(e) {

        e.preventDefault();

        // Process a fetch request from the correspondinf backend API
        // The header information "Content-type" is used to specifiy that the info being sent to the backend will be sent in the form JSON
        // The fetch request will communicate with our BE application providing it with a stringified JSON
        // Convert the info into JS object using .then => (res.json()) 
        fetch('http://localhost:4000/users/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            if(data.access !== undefined) {
                localStorage.setItem('token', data.access);
                retrieveUserDetails(data.access);

                Swol.fire({
                    title: "Login Successful",
                    icon: "success",
                    text: "Welcome to Zuitt!"
                });
            } else {
                Swol.fire({
                    title: "Login Successful",
                    icon: "error",
                    text: "Check your login details and try again!"
                })
            }
        })

        // Set the email of the authenticated user in the localStorage
        // localStorage.setItem('email', email);

        // Set the global user state to have properties obtained from local storage
        // Though access to the user information can be done via the localStorage this is necessary to update the user state which will help update the App component and rerender it to avoid refreshing the page upon user login and logout
        // When states change components are rerendered and the AppNavbar component will be updated based on the user credentials, unlike when using the localStorage where the localStorage does not trigger component rerendering

        // setUser({email: localStorage.getItem('email')});

        // Clear input fields after submission
        setEmail('');
        setPassword('');


        // alert('You are now logged in. 🥳🎉');

    }

    const retrieveUserDetails = (token) => {
        fetch('http://localhost:4000/users/details', {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            // Changes the global user state tos the 'id' and the 'isAdmin' property off the user which will be used for validation acrosss the whole application
            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        })
    }

    useEffect(() => {
        if((email !== '' && password !== '')) {
            setIsActive(true);
        } else {
            setIsActive(false);
        }
    }, [email, password]);

    return (

        // Conditional rendering will redirect the user to the courses page when a user is logged in
        (user.id !== null)
        ?
            <Navigate to='/courses'/>
        :
        
        <>
        <h1>Login</h1>
        <Form onSubmit={(e) => authenticate(e)}>
            <Form.Group controlId = "userEmail">
                <Form.Label> Email Address </Form.Label>
                <Form.Control
                    type="email"
                    placeholder = "Enter email"
                    required
                    value={email}
                    onChange={e => setEmail(e.target.value)}
                />
                <Form.Text className="text-muted">We'll never share your email with anyone else.</Form.Text>
            </Form.Group>

            <Form.Group controlId = "password">
                <Form.Label> Password </Form.Label>
                <Form.Control
                    type="password"
                    placeholder = "Password"
                    required
                    value={password}
                    onChange={e => setPassword(e.target.value)}
                />
            </Form.Group> <br/>

            {isActive
              ?
                <Button variant="success" type="submit" id="submitBtn"> Login </Button>
              :
                <Button variant="success" type="submit" id="submitBtn" disabled> Login </Button>
            }
        </Form>
        </>
    )
}