// import { Link } from 'react-router-dom';
// import  { Row, Col } from 'react-bootstrap';
import Banner from '../components/Banner'


export default function Error() {
	// return (
	// 		<Row>
	// 			<Col className="p-5, text-center">
	// 				{/*<img src="https://img.icons8.com/pastel-glyph/256/page-not-found--v2.png"/> */}
	// 				<h1>Page Not Found</h1>
	// 				back to the {""}
	// 				<Link to='/'>homepage</Link>
	// 			</Col>
	// 		</Row>
	// 	)

	const data = {
		title : "404 - Not Found",
		content : "The page you are looking for cannot be found",
		destination: "/",
		label: "Back home"
	}

	return (
		<>
		<Banner data={data}/>
		</>
	)
};
