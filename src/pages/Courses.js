// import coursesData from	'../data/coursesData';
import { useEffect, useState } from 'react';
import CourseCard from '../components/CourseCard';


export default function Courses() {

	// State that will be used to store the courses retrieved from the database
	const [ courses, setCourses ] = useState([]);

	//Retrieves the courses from the database ipon initial render of the "Courses" component
	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/courses`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			
			// Sets the "coruses" state to map the data retrieved form the fetch request into several "CourseCard" components
			setCourses(data.map(course => {
				return (
					<CourseCard key={course._id} courseProp = {course}/>
				)
			}))
		})
	}, [])

	// The "map" method loops through the individual course object in our array and retyrns a CourseCard component for each course

		// const courses = coursesData.map(course => {
		// 	return (
		// 		<CourseCard key={course.id} courseProp = {course}/>
		// 	)
		// })

	return (
		<>
			<h1>Courses</h1>
			{courses}
		</>
	)
}